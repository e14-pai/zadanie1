/* funkcja odposiada za ładowanie odpowiedniej podstrony WWW
	- name - zmienna, która odpowiada za nazwę przekazaną ze zeminnej-atrybutu przycisku HTML
	*/
function loadSite(name) {
	window.location = name + '.html';
}

/* funkcja sumująca 
	- a - parametr 1
	- b - parametr 2
	- zwraca sumę wartości a i b; 
	jeżeli chcemy, by sumowane były jedynie liczby należy dodać odpowiednie warunki
	takie jak np. isNaN() */
function sum(a, b) {
	return a + b;
}

/* funkcja dodająca zdarzenie do elementu/elementów reprezentowanych przez parametr s 
	- s - parametr mogący być nazwą elementu HTML, identyfikatorem bądź klasą 
	*/
function addEvents(s) {
	var btns = document.querySelectorAll(s); //funkcja wybierająca WSZYSTKIE elementy spełniające warunki/mające odpowiednią nazwę/klasę
	for (var i = 0; i < btns.length; i++) { 
	/* każdy wybrany element musi mieć oddzielenie indywiduwalnie nadaną funkcje nasłuchu 
		poniżej funkcja zakłada, że nadajemy obbsługę na kliknięcie (click); celem 
		nadania większej uniwersalności funkcji można przekazywać nazwę operacji zdarzenia
		jako dodatkowy parametr)
		*/
		btns[i].addEventListener('click', function(e) {
			/* ponieważ nadanie wartości zmiennej utworzononej w ramach wykonywanej funkcji
			   niszczy kod (zmienna żyje tylko do jej zakończenia, później występuje jako undefined)
			   poniższy kod pobiera interesującą nas wartość bezpośrednio z danych elementu*/
			var node = e.target; //najpierw pobieramy element HTML, na którym zostało wywołane zdarzenie (reprezentowane przez zmienną e)
			//poniższa pętla sprawdza czy atrybut data posiada zmienną name
			//jeżeli nie (będzie występować jako undefined) pobierany jest kolejnuy rodzic (element nadrzędny)
			//obecnie wybranego elementu HTML
			while (node.dataset.name === undefined)
				node = node.parentNode;
			loadSite(node.dataset.name); //cel zdarzenia -> załadowanie odpowiedniej strony WWW
		});
	}
}

